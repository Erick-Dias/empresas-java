package com.example.test.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

import com.example.test.model.User;
import com.example.test.repository.UserRepository;
import com.example.test.utils.KeyUtils;


@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private KeyUtils keyUtils;

    @Override
    public User register(User user) {
        ZonedDateTime date = ZonedDateTime.now(ZoneId.systemDefault());
        user.setPasswordHash(keyUtils.hashingPassword(user.getPasswordHash()));
        user.setCreated(date);
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public boolean removeUser(Long userId) throws IOException {

        Optional<User> optionalUser = userRepository.findById(userId);

        if (!optionalUser.isPresent())
            return false;

        userRepository.deactivateUser(false, userId);

        return true;
    }

    @Override
    @Transactional
    public boolean updateUser(Long userId, User user) throws IOException {
        ZonedDateTime date = ZonedDateTime.now(ZoneId.systemDefault());
        Optional<User> optionalUser = userRepository.findById(userId);

        if (!optionalUser.isPresent())
            return false;

        User newUser = userRepository.getById(optionalUser.get().getId());
        user.setPasswordHash(keyUtils.hashingPassword(user.getPasswordHash()));
        user.setUpdated(date);
        newUser = user;
        newUser.setId(optionalUser.get().getId());
        newUser.setCreated(optionalUser.get().getCreated());
        userRepository.save(newUser);

        return true;
    }
}
