package com.example.test.repository;

import java.util.Optional;

import com.example.test.model.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);
    Optional<User> findByName(String name);
    boolean existsByEmail(String email);

    @Modifying
    @Query("update User u set u.active = ?1 where u.id = ?2")
    void deactivateUser(Boolean isActive, Long id);
}