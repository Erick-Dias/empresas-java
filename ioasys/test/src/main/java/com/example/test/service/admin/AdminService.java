package com.example.test.service.admin;

import java.io.IOException;
import java.util.List;

import com.example.test.model.Admin;

public interface AdminService {
    Admin register(Admin user);
    List<Admin> findAllActiveAdmins();
    Admin findAdminById(Long email);
    Admin findAdminByName(String email);
    boolean updateAdmin(Long UserId, Admin admin) throws IOException;
    boolean removeAdmin(Long UserId) throws IOException;
}