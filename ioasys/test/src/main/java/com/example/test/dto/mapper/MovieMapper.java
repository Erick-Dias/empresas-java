package com.example.test.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import com.example.test.dto.MovieDTO;
import com.example.test.model.Movie;

public class MovieMapper {
    //Faz as conversões de User para UserDTO
    public static MovieDTO toMovieDto(Movie user) {
        return new MovieDTO()
            .setId(user.getId())
            .setAtores(user.getAtores())
            .setName(user.getName())
            .setDiretor(user.getDiretor())
            .setGenero(user.getGenero())
            .setPontuacao(user.getPontuacao());
    }

    //Faz as conversões de UserDTO para User
    public static Movie toMovie(MovieDTO userDto){
        return new Movie()
        .setId(userDto.getId())
        .setAtores(userDto.getAtores())
        .setName(userDto.getName())
        .setDiretor(userDto.getDiretor())
        .setGenero(userDto.getGenero())
        .setPontuacao(userDto.getPontuacao());
    }

    //Faz as conversões de Movies para MoviesDTO
    public static List<MovieDTO> toListMovieDTO(List<Movie> movies) {
        List<MovieDTO> movieDTO = new ArrayList<>();
        movies.forEach(u -> {
            movieDTO.add(new MovieDTO()
                .setId(u.getId())
                .setAtores(u.getAtores())
                .setName(u.getName())
                .setDiretor(u.getDiretor())
                .setGenero(u.getGenero())
                .setPontuacao(u.getPontuacao()));
        });

        return movieDTO;
    }
}
