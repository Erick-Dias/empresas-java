package com.example.test.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import com.example.test.dto.AdminDTO;
import com.example.test.model.Admin;

public class AdminMapper {
    //Faz as conversões de User para UserDTO
    public static AdminDTO toAdminDto(Admin admin) {
        return new AdminDTO()
                .setId(admin.getId())
                .setName(admin.getName())
                .setEmail(admin.getEmail());
    }

    //Faz as conversões de UserDTO para User
    public static Admin toAdmin(AdminDTO adminDto){
        return new Admin()
        .setEmail(adminDto.getEmail())
        .setName(adminDto.getName())
        .setId(adminDto.getId());
    }

    //Faz as conversões de Users para UsersDTO
    public static List<AdminDTO> toListAdminDTO(List<Admin> admins) {
        List<AdminDTO> adminDTO = new ArrayList<>();
        admins.forEach(u -> {
            adminDTO.add(new AdminDTO()
                .setEmail(u.getEmail())
                .setName(u.getName())
                .setId(u.getId()));
        });

        return adminDTO;
    }
}
