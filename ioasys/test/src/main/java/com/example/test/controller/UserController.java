package com.example.test.controller;

import java.io.IOException;

import com.example.test.dto.UserDTO;
import com.example.test.dto.mapper.UserMapper;
import com.example.test.model.User;
import com.example.test.repository.UserRepository;
import com.example.test.service.user.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    
    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/")
    public ResponseEntity<?> registerUser(@RequestBody UserDTO userDTO) {
        log.info(">>>> Trying to create a User");
        if (userRepository.existsByEmail(userDTO.getEmail()))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This email its already in use");
        User user = userService.register(UserMapper.toUser(userDTO));
        return ResponseEntity.ok(UserMapper.toUserDto(user));

    }

    @PostMapping("/update")
    public ResponseEntity<Boolean> updateUser(@RequestParam("user_id") Long userId, @RequestBody UserDTO user){
        log.info(">>>> Trying to update a User");
        try {
            return ResponseEntity.ok(userService.updateUser(userId, UserMapper.toUser(user)));
        } catch (IOException ioe) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/")
    public ResponseEntity<Boolean> removeUser(@RequestParam("user_id") Long userId){
        log.info(">>>> Trying to remove a User");
        try {
            return ResponseEntity.ok(userService.removeUser(userId));
        } catch (IOException ioe) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}