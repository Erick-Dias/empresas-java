package com.example.test.controller;

import com.example.test.dto.MovieDTO;
import com.example.test.dto.mapper.MovieMapper;
import com.example.test.exceptions.GlobalException;
import com.example.test.model.Movie;
import com.example.test.repository.MovieRepository;
import com.example.test.service.movie.MovieService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/movie")
public class MovieController {
    
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieService movieService;

    @PostMapping("/")
    public ResponseEntity<?> registerMovie(@RequestBody MovieDTO movieDTO) {
        log.info(">>>> Starting to insert a new movie into the database");
        if (movieRepository.existsById(movieDTO.getId()))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This movie was alredy inserted");
        Movie movie = movieService.register(MovieMapper.toMovie(movieDTO));
        return ResponseEntity.ok(MovieMapper.toMovieDto(movie));

    }

    @PostMapping("/score/")
    public ResponseEntity<?> reviewMovie(@RequestParam("score") int score, @RequestParam("id") Long id) {
        log.info(">>>> Trying to review a movie");
        if (score > 4 || score < 1)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid score");
        movieService.reviewMovie(score, id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/get-movies/{id}")
    public ResponseEntity<?> getMovie(@RequestParam("name") String name) throws GlobalException {
        log.info(">>>> Trying to retrive movies");
        if (!movieRepository.existsByName(name))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("It wanst possible to find any movies");
        return ResponseEntity.ok(MovieMapper.toListMovieDTO(movieService.getMovies()));
    }
}