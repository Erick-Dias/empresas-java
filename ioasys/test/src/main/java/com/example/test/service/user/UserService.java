package com.example.test.service.user;

import java.io.IOException;
import com.example.test.model.User;


public interface UserService {
    User register(User user);
    boolean removeUser(Long UserId) throws IOException;
    boolean updateUser(Long UserId, User user) throws IOException;
}