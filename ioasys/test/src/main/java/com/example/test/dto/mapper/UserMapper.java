package com.example.test.dto.mapper;

import com.example.test.dto.UserDTO;
import com.example.test.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserMapper {
    //Faz as conversões de User para UserDTO
    public static UserDTO toUserDto(User user) {
        return new UserDTO()
                .setId(user.getId())
                .setName(user.getName())
                .setEmail(user.getEmail());
    }

    //Faz as conversões de UserDTO para User
    public static User toUser(UserDTO userDto){
        return new User()
        .setEmail(userDto.getEmail())
        .setName(userDto.getName())
        .setId(userDto.getId());
    }

    //Faz as conversões de Users para UsersDTO
    public static List<UserDTO> toListUserDTO(List<User> users) {
        List<UserDTO> usersDTO = new ArrayList<>();
        users.forEach(u -> {
            usersDTO.add(new UserDTO()
                .setEmail(u.getEmail())
                .setName(u.getName())
                .setId(u.getId()));
        });

        return usersDTO;
    }
}
