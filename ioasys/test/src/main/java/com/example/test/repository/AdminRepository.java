package com.example.test.repository;

import java.util.Optional;

import com.example.test.model.Admin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface AdminRepository extends JpaRepository<Admin, Long> {
    Optional<Admin> findByEmail(String email);
    Optional<Admin> findByName(String name);
    boolean existsByEmail(String email);

    @Modifying
    @Query("update Admin a set a.active = ?1 where a.id = ?2")
    void deactivateAdmin(Boolean isActive, Long id);
}