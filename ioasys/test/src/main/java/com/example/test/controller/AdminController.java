package com.example.test.controller;

import java.io.IOException;
import java.util.List;

import com.example.test.dto.AdminDTO;
import com.example.test.dto.mapper.AdminMapper;
import com.example.test.model.Admin;
import com.example.test.repository.AdminRepository;
import com.example.test.service.admin.AdminService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/admin")
public class AdminController {
    
    @Autowired
    private AdminService adminService;

    @Autowired
    private AdminRepository adminRepository;

    @PostMapping("/")
    public ResponseEntity<?> registerAdmin(@RequestBody AdminDTO AdminDTO) {
        log.info(">>>> Trying to register a new Admin");
        if (adminRepository.existsByEmail(AdminDTO.getEmail()))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This email its already in use");
        Admin admin = adminService.register(AdminMapper.toAdmin(AdminDTO));
        return ResponseEntity.ok(AdminMapper.toAdminDto(admin));

    }

    @GetMapping("/id/{id}")
    public ResponseEntity<?> getAdminById(@PathVariable("id") Long id) {
        log.info(">>>> Trying to retrive an Admin by ID");
        if (!adminRepository.existsById(id))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Admin not found");

        return ResponseEntity.ok(AdminMapper.toAdminDto(adminService.findAdminById(id)));
    }

    @GetMapping("/")
    public ResponseEntity<?> getAllActiveAdmins() {
        log.info(">>>> Trying to retrive all active Admins");
        List<Admin> Admins = adminService.findAllActiveAdmins();
        if (Admins.isEmpty())
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Couldnt retrive any admins");

        return ResponseEntity.ok(AdminMapper.toListAdminDTO(Admins));
    }

    @GetMapping("/getByName/{name}")
    public ResponseEntity<?> getAdminByName(@PathVariable("name") String name) {
        log.info(">>>> Trying to retrive an Admin by name");
        Admin admin = adminService.findAdminByName(name);
        if (admin == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Admin name " + name + " not found");

        return ResponseEntity.ok(AdminMapper.toAdminDto(admin));
    }

    @DeleteMapping("/")
    public ResponseEntity<Boolean> removeAdmin(@RequestParam("Admin_id") Long AdminId){
        log.info(">>>> Trying to remove an Admin");
        try {
            return ResponseEntity.ok(adminService.removeAdmin(AdminId));
        } catch (IOException ioe) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}