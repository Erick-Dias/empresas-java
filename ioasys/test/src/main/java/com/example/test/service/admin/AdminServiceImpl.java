package com.example.test.service.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.example.test.model.Admin;
import com.example.test.repository.AdminRepository;
import com.example.test.utils.KeyUtils;


@Component
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminRepository adminRepository;
    
    @Autowired
    private KeyUtils keyUtils;

    @Override
    public Admin register(Admin admin) {
        ZonedDateTime date = ZonedDateTime.now(ZoneId.systemDefault());
        admin.setPasswordHash(keyUtils.hashingPassword(admin.getPasswordHash()));
        admin.setCreated(date);
        return adminRepository.save(admin);
    }

    @Override
    public Admin findAdminById(Long id) {
        Optional<Admin> optionalSession = adminRepository.findById(id);

        if (optionalSession.isPresent()) {
            return optionalSession.get();
        } else {
            return null;
        }
    }

    @Override
    public Admin findAdminByName(String name) {
        Optional<Admin> optionalSession = adminRepository.findByName(name);

        if (optionalSession.isPresent()) {
            return optionalSession.get();
        } else {
            return null;
        }
    }

    @Override
    public List<Admin> findAllActiveAdmins() {
       return Optional.ofNullable(adminRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                                        .stream()
                                        .filter(user -> user.getActive() == true)
                                        .collect(Collectors.toList())).orElse(new ArrayList<>());
    }

    @Override
    @Transactional
    public boolean removeAdmin(Long userId) throws IOException {

        Optional<Admin> optionalUser = adminRepository.findById(userId);

        if (!optionalUser.isPresent())
            return false;

        adminRepository.deactivateAdmin(false, userId);

        return true;
    }

    @Override
    public boolean updateAdmin(Long userId, Admin admin) throws IOException {
        ZonedDateTime date = ZonedDateTime.now(ZoneId.systemDefault());
        Optional<Admin> optionalUser = adminRepository.findById(userId);

        if (!optionalUser.isPresent())
            return false;

        Admin newAdmin = adminRepository.getById(optionalUser.get().getId());
        admin.setPasswordHash(keyUtils.hashingPassword(admin.getPasswordHash()));
        admin.setUpdated(date);
        newAdmin = admin;
        newAdmin.setId(optionalUser.get().getId());
        newAdmin.setCreated(optionalUser.get().getCreated());
        adminRepository.save(newAdmin);

        return true;
    }
}
