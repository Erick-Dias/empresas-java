package com.example.test.service.movie;

import java.util.List;

import com.example.test.exceptions.GlobalException;
import com.example.test.model.Movie;


public interface MovieService {
    Movie register(Movie user);
    List<Movie> getMovies() throws GlobalException;
    boolean reviewMovie(int score, Long id);
}