package com.example.test.service.movie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

import com.example.test.exceptions.GlobalException;
import com.example.test.model.Movie;
import com.example.test.repository.MovieRepository;


@Component
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public Movie register(Movie movie) {
        ZonedDateTime date = ZonedDateTime.now(ZoneId.systemDefault());
        movie.setCreated(date);
        return movieRepository.save(movie);
    }

    @Override
    public boolean reviewMovie(int score, Long id) {
        Optional<Movie> optionalUser = movieRepository.findById(id);
        ZonedDateTime date = ZonedDateTime.now(ZoneId.systemDefault());

        if (!optionalUser.isPresent())
            return false;

        Movie newAdmin = movieRepository.getById(optionalUser.get().getId());
        optionalUser.get().getPontuacaoHistorico().add(score);
        newAdmin.setPontuacaoHistorico(optionalUser.get().getPontuacaoHistorico());

        OptionalDouble optionalScore = optionalUser.get().getPontuacaoHistorico().stream().mapToDouble(Integer::doubleValue).average();
        if (!optionalScore.isPresent())
            return false;

        newAdmin.setPontuacao(optionalScore.getAsDouble());
        newAdmin.setId(optionalUser.get().getId());
        newAdmin.setCreated(optionalUser.get().getCreated());
        newAdmin.setUpdated(date);
        movieRepository.save(newAdmin);
        return true;
    }

    @Override
    public List<Movie> getMovies() throws GlobalException {
        try {
            return Optional.ofNullable(movieRepository.findAll(Sort.by(Sort.Direction.DESC, "pontuacao"))).orElse(new ArrayList<>());
        }catch(Exception e) {
            throw new GlobalException("Error while trying to find the movies");
        }
    }

}
