PASSOS PARA EXECUÇÃO DO CODIGO:
	1 - suba o docker dentro do test-docker-master via docker-compose
	2 - abra o painel do Keycloak - http://localhost:8089/auth/admin/master/console/#/realms/test/users - usando as credenciais (user: admin, pwd: admin)
	3 - navegue até a sessão de Manage > Users no menu dropdown à esquerda e selecione a opção add user 
	4 - crie dois usuários, admin-service e user, e atribua uma senha igual aos seus nomes reespectivos na aba credentials. IMPORTANTE: desabilitar opção de senha temporaria na hora da atribuição da senha
	5 - ainda dentro do usuário atribua a role reespectiva para cada um dos usuários (user = USER, admin-service = ADMIN-SERVICE) na aba de Role Mapping
	6 - agora na sessão Clients no menu dropdown selecione o Client ID sboot-service
	7 - Na aba de Credentials clique no botão Regenerate secret e copie a secret gerada
	8 - importe a colection cedida no postman
	9 - dentro do postman, em cada um dos endpoints será necessário subistituir o campo de Client Secret pela secret gerada no passo anterior
	10 - gere un novo token para o endpoint desejado e execute o mesmo.